import Vue from 'vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import VueMeta from 'vue-meta'
import App from './App.vue'
import router from './router'
import store from './store'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import ToggleButton from 'vue-js-toggle-button'
import Notifications from 'vue-notification'
import { io } from 'socket.io-client'
import VueApexCharts from 'vue-apexcharts'

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
Vue.use(VueMeta)
Vue.use(ToggleButton)
Vue.use(Notifications)
Vue.use(VueApexCharts)

Vue.config.productionTip = false

new Vue({
  data () {
    return {
      socket: io(process.env.API_URL || 'http://localhost:8100')
    }
  },
  router,
  store,
  render: h => h(App)
}).$mount('#app')
