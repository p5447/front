import ApiService from "./ApiService";

const RoomService = {
  async getRooms() {
    const response = await ApiService.get(`/room`);
    return response;
  },
  async getRoomsByUser() {
    const response = await ApiService.get(`/room/by-user`);
    return response;
  },
  async getRoomById(roomId) {
    const response = await ApiService.get(`/room/${roomId}`);
    return response;
  },
  async createRoom(payload) {
    const response = await ApiService.post(`/room`, { ...payload });
    return response;
  },
  async deleteRoom(payload) {
    const response = await ApiService.delete(`/room`, { ...payload });
    return response;
  },
};

export default RoomService;
