const ValidateService = {
  mapUserFormError(key, value, matchValue = null) {
    switch (key) {
      case "email":
      case "name":
      case "tel":
        if (!value) return `${key} is required.`;
        return "";
      case "password":
        if (!value) return `${key} is required.`;
        if (value.trim().length < 8)
          return `Password should be at least 8 character.`;
        return "";
      case "re_password":
        if (!value) return `${key} is required.`;
        else if (value !== matchValue) return `Re password not match password.`;
        return "";
      default:
        return "";
    }
  },
  mapCompanyFormError(key, value) {
    switch (key) {
      case "name":
      case "specify":
        if (!value) return `${key} is required.`;
        return "";
      default:
        return "";
    }
  },
  mapQueueTypeFormError(key, value) {
    switch (key) {
      case "title":
        if (!value) return `${key} is required.`;
        return "";
      default:
        return "";
    }
  },
};

export default ValidateService;
