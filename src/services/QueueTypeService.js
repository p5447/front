import ApiService from "./ApiService";

const QueueTypeService = {
  async getQueueTypes() {
    const response = await ApiService.get("/queue-type");
    return response;
  },
  async createQueueType(payload) {
    const response = await ApiService.post(`/queue-type`, { ...payload });
    return response;
  },
  async editQueueType(payload) {
    const response = await ApiService.put(`/queue-type`, { ...payload });
    return response;
  },
  async deleteQueueType(payload) {
    const response = await ApiService.delete(`/queue-type`, { ...payload });
    return response;
  },
};

export default QueueTypeService;
