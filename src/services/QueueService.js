import ApiService from "./ApiService";

const QueueService = {
  async getQueueById(queueId) {
    const response = await ApiService.get(`/queue/${queueId}`);
    return response;
  },
  async createQueue(queuePayload) {
    const response = await ApiService.post("/queue", queuePayload);
    return response;
  },
  async nextQueue(nextQueuePayload) {
    const response = await ApiService.post(
      "/queue/next-queue",
      nextQueuePayload
    );
    return response;
  },
  async getForwardQueue(roomId = "all") {
    const response = await ApiService.get(`/queue/forward-queue/${roomId}`);
    return response;
  },
};

export default QueueService;
