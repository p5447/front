import axios from "axios";
import AuthService from "./AuthService";

const API_URL = process.env.API_URL || "http://localhost:8100";

const axiosInstance = axios.create({
  baseURL: API_URL,
});

axiosInstance.interceptors.response.use(
  (response) => response,
  async (error) => {
    const originalRequest = error.config;
    if (
      (error.response?.status !== 200 &&
        originalRequest.url === "/auth/token" &&
        originalRequest.method === "post") ||
      (error.response?.status !== 200 && originalRequest._retry)
    ) {
      localStorage.clear();
      window.location.href = "/login";
      return Promise.reject(error);
    }

    if (error?.response?.status === 401 && !originalRequest._retry) {
      const refreshResponse = await AuthService.refreshToken({
        email: localStorage.getItem("user")
          ? JSON.parse(localStorage.getItem("user")).email
          : "",
        refreshToken: localStorage.getItem("refreshToken"),
      });

      localStorage.setItem("token", refreshResponse.data?.token || "");
      error.config.headers["authorization"] = refreshResponse.data?.token || "";

      return axios.request(error.config);
    }

    return Promise.reject(error);
  }
);

const ApiService = {
  company() {
    if (
      localStorage.getItem("currentPage") === "QueueRegister" ||
      localStorage.getItem("currentPage") === "Dashboard" ||
      localStorage.getItem("currentPage") === "QueueMonitoring"
    ) {
      return localStorage.getItem("companySelected")
        ? JSON.parse(localStorage.getItem("companySelected"))
        : null;
    } else {
      return localStorage.getItem("company")
        ? JSON.parse(localStorage.getItem("company"))
        : null;
    }
  },
  header() {
    const company = this.company();
    const header = {
      authorization: localStorage.getItem("token"),
      company: company ? company._id : null,
    };
    return header;
  },
  async get(url) {
    const response = await axiosInstance
      .get(`${url}`, { headers: this.header() })
      .then((res) => {
        return res.data;
      })
      .catch((err) => {
        console.log(err);
        return err;
      });

    return response;
  },

  async post(url, body = {}) {
    const response = await axiosInstance
      .post(`${url}`, body, { headers: this.header() })
      .then((res) => {
        return res.data;
      })
      .catch((err) => {
        console.log(err);
        return err;
      });

    return response;
  },

  async put(url, body = {}) {
    const response = await axiosInstance
      .put(`${url}`, body, { headers: this.header() })
      .then((res) => {
        return res.data;
      })
      .catch((err) => {
        console.log(err);
        return err;
      });

    return response;
  },

  async delete(url, body = {}) {
    const response = await axiosInstance
      .delete(`${url}`, { data: body, headers: this.header() })
      .then((res) => {
        return res.data;
      })
      .catch((err) => {
        console.log(err);
        return err;
      });

    return response;
  },
};

export default ApiService;
