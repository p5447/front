import ApiService from "./ApiService";
const CompanyService = {
  async getCompanies() {
    const response = await ApiService.get("/company");
    return response;
  },
  async getCompanyById(companyId) {
    const response = await ApiService.get(`/company/${companyId}`);
    return response;
  },
  async createCompany(payload) {
    const response = await ApiService.post("/company", { ...payload });
    return response;
  },
  async deleteCompany(payload) {
    const response = await ApiService.delete(`/company`, { ...payload });
    return response;
  },
};

export default CompanyService;
