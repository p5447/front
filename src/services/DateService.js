import moment from "moment";
import DateConstant from "../constants/DateConstant";

const DateService = {
  toThaiDate(date) {
    const dateUTC = moment(date).utcOffset(420);
    const year = dateUTC.year();
    const thaiDate = dateUTC
      .format(DateConstant.DEFAULT_FORMAT_DATE)
      .replace(`${year}`, `${year + 543}`);

    return thaiDate;
  },

  toThaiDateDisplay(date) {
    const thaiDate = this.toThaiDate(date);
    const thaiDateSplit = thaiDate.split(" ");
    return `วันที่ ${thaiDateSplit[0]} เวลา ${thaiDateSplit[1]} น.`;
  },
};

export default DateService;
