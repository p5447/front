import ApiService from "./ApiService";

const DashboardService = {
  async getDashboard(payload) {
    const response = await ApiService.post("/dashboard", { ...payload });
    return response;
  },
  async getDashboardAdmin(payload) {
    const response = await ApiService.post("/dashboard/admin", { ...payload });
    return response;
  },
};

export default DashboardService;
