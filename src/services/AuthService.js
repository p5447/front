import ApiService from "./ApiService";

const AuthService = {
  async login(payload) {
    const response = await ApiService.post("/auth/login", { ...payload });
    return response;
  },
  async refreshToken(payload) {
    const response = await ApiService.post("/auth/token", { ...payload });
    return response;
  },
};

export default AuthService;
