import ApiService from "./ApiService";
const UserService = {
  async getUsers(companyId) {
    const response = await ApiService.get(
      companyId ? `/user/${companyId}` : `/user`
    );
    return response;
  },
  async getUser(userId) {
    const response = await ApiService.get(`/user/user/${userId}`);
    return response;
  },
  async createUser(payload) {
    const response = await ApiService.post("/user", { ...payload });
    return response;
  },
  async editUser(payload) {
    const response = await ApiService.put("/user", { ...payload });
    return response;
  },
  async resetPassword(payload) {
    const response = await ApiService.put("/user/reset-password", {
      ...payload,
    });
    return response;
  },
  async deleteUser(payload) {
    const response = await ApiService.delete("/user", { ...payload });
    return response;
  },
};

export default UserService;
