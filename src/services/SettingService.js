import ApiService from "./ApiService";

const SettingService = {
  async isOpenQueue() {
    const response = await ApiService.get(`/setting/queue`);
    return response;
  },
  async openQueue() {
    const response = await ApiService.post(`/setting/open-queue`);
    return response;
  },
  async closeQueue() {
    const response = await ApiService.post(`/setting/close-queue`);
    return response;
  },
};

export default SettingService;
