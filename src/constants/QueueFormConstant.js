const QueueFormConstant = {
  SELECT_QUEUE_TYPE: "SELECT_QUEUE_TYPE",
  PRESS_INFO: "PRESS_INFO",
  NOT_FOUND_ROOM: "NOT_FOUND_ROOM",
};

export default QueueFormConstant;
