const DateConstant = {
  DEFAULT_FORMAT_DATE: "DD/MM/YYYY HH:mm:ss",
};

export default DateConstant;
