import Vue from "vue";
import VueRouter from "vue-router";
import QueueRegister from "../views/QueueRegister.vue";
import QueueDetail from "@/views/QueueDetail";
import RoomManage from "@/views/RoomManage";
import QueueTypeManage from "@/views/QueueTypeManage";
import Setting from "@/views/Setting";
import QueueMonitoring from "@/views/QueueMonitoring";
import Login from "@/views/Login";
import CompanyManage from "@/views/CompanyManage";
import UserManage from "@/views/UserManage";
import SuperUserManage from "@/views/SuperUserManage";
import Dashboard from "@/views/Dashboard";
import Profile from "@/views/Profile";
import NotFound from "@/views/NotFound";
import MyQR from "@/views/MyQR";

Vue.use(VueRouter);

const routes = [
  {
    path: "/login",
    name: "Login",
    component: Login,
    meta: { layout: false },
  },
  {
    path: "/queue-register",
    name: "QueueRegister",
    component: QueueRegister,
    meta: { layout: false },
  },
  {
    path: "/not-found",
    name: "NotFound",
    component: NotFound,
    meta: { layout: false },
  },
  {
    path: "/queue",
    name: "QueueDetailNoId",
    component: QueueDetail,
    meta: { layout: false },
  },
  {
    path: "/queue/:queueId",
    name: "QueueDetail",
    component: QueueDetail,
    meta: { layout: false },
  },
  {
    path: "/queue-type",
    name: "QueueTypeManage",
    component: QueueTypeManage,
    meta: { layout: true, auth: true, permission: ["admin", "user"] },
  },
  {
    path: "/queue-monitoring",
    name: "QueueMonitoring",
    component: QueueMonitoring,
    meta: {
      layout: true,
    },
  },
  {
    path: "/room",
    name: "RoomManage",
    component: RoomManage,
    meta: { layout: true, auth: true, permission: ["admin", "user"] },
  },
  {
    path: "/setting",
    name: "Setting",
    component: Setting,
    meta: { layout: true, auth: true, permission: ["admin"] },
  },
  {
    path: "/company",
    name: "CompanyManage",
    component: CompanyManage,
    meta: {
      layout: true,
      auth: true,
      permission: ["super_admin"],
    },
  },
  {
    path: "/company/:companyId/user",
    name: "UserManageWithCompany",
    component: UserManage,
    meta: { layout: true, auth: true, permission: ["super_admin"] },
  },
  {
    path: "/user",
    name: "SuperUserManage",
    component: SuperUserManage,
    meta: { layout: true, auth: true, permission: ["super_admin", "admin"] },
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    component: Dashboard,
    meta: {
      layout: true,
      auth: true,
      permission: ["super_admin", "admin", "user"],
    },
  },
  {
    path: "/profile",
    name: "Profile",
    component: Profile,
    meta: {
      layout: true,
      auth: true,
      permission: ["super_admin", "admin", "user"],
    },
  },
  {
    path: "/my-qr",
    name: "MyQR",
    component: MyQR,
    meta: { layout: true, auth: true, permission: ["admin", "user"] },
  },
  {
    path: "*",
    redirect: { name: "Dashboard" },
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  try {
    if (to.meta.auth) {
      const user = localStorage.getItem("user")
        ? JSON.parse(localStorage.getItem("user"))
        : null;
      const company = localStorage.getItem("company");
      if (!user) next({ name: "Login" });
      if (user.permission !== "super_admin" && !company)
        next({ name: "Login" });
      if (to.meta.permission.indexOf(user.permission) === -1)
        next({ name: "Login" });

      localStorage.setItem("currentPage", to.name);
      next();
    } else {
      localStorage.setItem("currentPage", to.name);
      next();
    }
  } catch (error) {
    next({ name: "Login" });
  }
});

export default router;
