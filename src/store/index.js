import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    account: localStorage.getItem("user")
      ? JSON.parse(localStorage.getItem("user"))
      : null,
  },
  mutations: {
    setAccount(state, payload) {
      state.account = payload;
      if (payload) localStorage.setItem("user", JSON.stringify(payload));
    },
  },
  actions: {},
  modules: {},
});
